source( "options.R" )
source( "Functions.R" )
source( "tasks/TaskFunctions.R" )
library("moments")
library("maxLik")

    taskId = FIRST_TASK_ID;
    alpha1 = 0.05;
    a = 3.6;
    b = 5.2;
    lambda0 = 4.0;
    lambda1 = 5.0;
    stepHistogram = 1.0
    
    
    #Загрузка файла
    currentFileName = 'data.txt' 
    file_path = paste( 'data/', currentFileName, sep='')
    data = getDataArray( file_path )
    printVar( "data:", data )
    startParametrs = c(1)
    print( "Задача 1")
    taskId = FIRST_TASK_ID ;
    printVar( "", "Задача a")
    source( "tasks/A.R" )
    
    printVar( "", "Задача b")
    start = a;
    end = b;
    source( "tasks/B.R" )
    printVar( "" , "Задача c")
    source( "tasks/C.R" )
    
    printVar( "", "Задача d")
    alpha = alpha1;
    fisher_function_id = POSION_LAMDA_FISHER_FUNCTION 
    source( "tasks/D.R" )
    
    printVar( "", "Задача e")
    parametrs = array(lambda0)
    source( "tasks/E.R" )
    
    printVar( "", "Задача f")
    source( "tasks/F.R" )
    printVar( "", "Задача g")
   # source( "tasks/G.R" )
    
    #Задача h
    
    taskId = H_FIRST_TASK_ID ;
   print( "Задача c")
    source( "tasks/C.R" )
    
   printVar( "", "Задача d")
    alpha = alpha1;
    fisher_function_id = POSION_LAMDA_FISHER_FUNCTION 
    source( "tasks/D.R" )
    
   printVar( "", "Задача e")
    parametrs = array(lambda0)
    source( "tasks/E.R" )

   printVar( "", "Задача f")
    source( "tasks/F.R" )
  #Загрузка файла
  currentFileName = 'data2.txt' 
  file_path = paste( 'data/', currentFileName, sep='')
  data = getDataArray( file_path )
  print( data )
  taskId = SECOND_TASK_ID 
  h = 2.00;
  a0 = 5.00;
  sigma = 5.00;
  alpha2= 0.01;
  stepHistogram = h;
  printVar( "", "Задача a")
  source( "tasks/A.R" )
  
  printVar( "", "Задача b")
  start = a;
  end = b;
  source( "tasks/B.R" )
  
  printVar("", "Задача c")
  startParametrs = c(a0, sigma)
  
  source( "tasks/C.R" )
  
  
  alpha = alpha2;
  fisher_function_id = NORM_A_FISHER_FUNCTION 
  printVar( "", "Задача d")
  source( "tasks/D.R" )
  
  alpha = alpha2;
  fisher_function_id = NORM_SIGMA_A_FISHER_FUNCTION 
  source( "tasks/D.R" )
  
  printVar( "", "Задача e")
  parametrs = array(a0, sigma^2);
  source( "tasks/E_2.R" )
  
  
  #Задача f 
  printVar( "", "Задача f")
  parametrs = array(a0, sigma^2);
  source( "tasks/E.R" )
  
  
  printVar( "", "Задача f")
  source( "tasks/F.R" )
  


