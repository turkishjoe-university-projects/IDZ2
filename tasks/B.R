#исходные данные data
meanValue = mean(data)
printVar( 'Viburochnoe srednee:', meanValue , 2 )
varianceValue = sd( data )
printVar( 'Viburochnaya dispersia', varianceValue , 2 )

#Подсчёт медианы
mediaValue = median( data )
printVar( 'Mediana :', mediaValue , 2 )

skewnessValue = skewness(data)
printVar( 'Assimetria:', skewnessValue  , 2 )

kurtosisValue= kurtosis(data)
printVar( 'Ecsess:', kurtosisValue , 2 )

probability = getProbability( data, end  ) - getProbability( data, start)  ;
printVar( 'probability:', probability , 2 )
