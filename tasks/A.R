 #Построение вариационного ряда эмпирерической фунции и гисторграммы частот
#Вход : Массив data

#вариацонный ряд 
par(mfrow=c(1,1))
maxValue = as.integer( max(data) ) +stepHistogram
printVar( "maxValue:", maxValue, 1 )

minValue=as.integer( min(data ) ) - stepHistogram
printVar( "minValue:", minValue, 1 )

ranges = getRanges( minValue, maxValue, stepHistogram );
histinfo = hist( data , breaks = ranges, freq=TRUE, right=TRUE)
plot(  histinfo$mids, histinfo$counts, type = 'line')
empericalFunction = vector()
empericalFunction[1]=0;
for( i in 1:length(histinfo$density) )
{
  empericalFunction[i + 1]=empericalFunction[i] + histinfo$density[ i ] # Создание эмпирической функции
}
printVar( "empericalFunction:", empericalFunction, 1 )
plot.ecdf(data)
#plot(data, empericalFunction )
