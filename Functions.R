isInVector = function( vec, element )
{
  for( i in 1:length(vec) )
  {
    if( vec[i] == element  )
      return ( TRUE )
  }
  return ( FALSE )
}

PRODUCTION_VAR = 0 
if( !exists('CURRENT_DEBUG_LEVEL') )
  CURRENT_DEBUG_LEVEL = PRODUCTION_VAR ;
printVar = function( name, value, debugLevel = PRODUCTION_VAR )
{
  if( ( !is.vector( CURRENT_DEBUG_LEVEL) && CURRENT_DEBUG_LEVEL == debugLevel )
      || isInVector( CURRENT_DEBUG_LEVEL, debugLevel ) )
  {
  
    if( name != "" )
      print( paste( name , ":" ) )
    print( value )  
  }
  
}


getDataArray = function( fileName )
{
  table = read.table( fileName,  encoding="UTF-8", stringsAsFactors=FALSE  )
  data = table$V1;
  printVar("ho accept", "" , 101)
  data2=vector();
  #print(data)
  data2 = as.numeric( data )
  #print(data2)
  return ( data2 )
}

getRangeForHi = function( histinfo, densityF, parametrs  )
{  
  freqs = histinfo$counts
  ranges = histinfo$breaks
  n1 = length( ranges ) - 1
  r = n1 - 1
  p = array( dim = n1  )
  printVar("ranges", ranges , 101)
  printVar("parametrs", parametrs , 101)
  printVar("densityF", densityF , 101)
  
  p[1] = densityF( ranges[1], parametrs )
  p[2:r] = densityF( ranges[2:r], parametrs ) -  densityF( ranges[1:(r-1)], parametrs  )
  p[r+1]=1-densityF( ranges[r], parametrs )
  printVar("p", p , 101)
  
  return ( p )
}

countSimularData = function( data, element )
{
  cnt = 0 
  for( i in 1:length( data ) )
  {
    if( data[ i ] == element )
    {
      cnt = cnt + 1 
    }
  }
  return ( cnt ) ;
}

getSeries = function( data )
{
  data = sort(data);
  result = data.frame();
  j = 1 
  i = 1
  n = length( data )
  
  while( i <= n  )
  {
    result[ "key", j  ] = j 
    result[ "count", j  ] = countSimularData( data, data[ i ] ) 
    i = i + result[ "count", j  ] 
    j = j + 1 ;
    #result = c( result, as.vector( tmpresult ) ) 
  }
  return ( result )
}


getPolygonFreq = function( data )
{
  data = sort(data);
  result = vector();
  j = 1 
  i = 1
  while( i < length( data ) )
  {
      result[ j ] = countSimularData( data, data[ i ] )
      i = i + 1;
      j = j + 1 ;
  }
  return ( result )
}

getTrustRange = function( maxGrade, fisherFunction, alpha )
{
  result = array();
  quantile = 1-alpha/2;
  tmp = quantile * sqrt( fisherFunction( maxGrade ) ) 
  result[ 1 ] = maxGrade - tmp;
  result[ 2 ] = maxGrade + tmp;
  
  return (result)
}

getRanges = function( start, end, step )
{
  ranges = vector();
  j = 1 ;
  ranges[1]= i = start ;
  while( ranges[j] < end )
  {
    j = j + 1 ;
    i = i + step ;
    ranges[j] = i ;
  }
  return ( ranges )
}

getProbability = function( x, t )
{
   z = x[x<t];
   result = length( z )/length( x )
   return (result)
}

